# micat

Micat is for Microscopy Calibration and Testing using USAF resolution targets.

> *micat* - The third-person singular present active indicative of *micō*  
> *micō* - Twinkle (Latin)

## Install

You can install `micat` for python 3.6 and 3.7 with:

    pip install git+https://gitlab.com/bath_open_instrumentation_group/micat.git

we will support python 3.8 once `scipy` can be pip installed on 3.8 successfully.


## Using micat

Micat will eventually port all the functionality from [Richard's USAF analysis repository](https://github.com/rwb27/usaf_analysis). So far we have completed:

`usafcal` - This takes an image of a USAF resolution target and calculates the effective size of one pixel in the image, and also the field of view. These are output in a PDF. Unless told otherwise the script assumes that the smallest element it can find is group 7 element 6. **You should check the pdf output to check if it has labelled the groups correctly**. If not you can run the analysis again using the `-g` and `-e` flags to set the group correct smallest group. The as standard the pdf will have the same name (but different extension) and directory as the input image. To use should run the following command from a terminal

    micat usafcal filename.jpg

or to specify the smallest element analysed is element 2 in group 4:

    micat usafcal filename.jpg -g 4 -e 2

if you want to run multiple files you can input multiple file names:

    micat usafcal filename1.jpg filename2.jpg

or if you terminal supports it you can use wild cards

    micat usafcal */*.jpg

If you want more information on the progress you can use a the verbose flag -v

    micat usafcal filename.jpg -v

And if you want a complete list of options you can run

    micat help usafcal


