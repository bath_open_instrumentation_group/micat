
"""
Analyse a USAF test target image, to determine the image's dimensions.

(c) Richard Bowman and Julian Stirling 2019
Released under GNU GPL v3

See: https://en.wikipedia.org/wiki/1951_USAF_resolution_test_chart
From Wikipedia, the number of line pairs/mm is 2^(g+(h-1)/6) where g is the
"group number" and h is the "element number".  Each group has 6 elements, 
numbered from 1 to 6.  A ThorLabs test target goes down to group 7, meaning
the finest pitch is 2^(7+(6-1)/6)=2^(47/8)=

"""

import sys
import argparse
import numpy as np
from skimage.io import imread
import cv2
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches
import yaml

class USAFmatch():
    """Class for each element found ing an image of a USAF target"""
    def __init__(self, image, score,location,template,horizontal):
        """Initalise with the input image, the score of the match, the position in x and y,
        the template, and whether the region is horizontal bars"""
        
        self.template = template
        self.score = score
        self.x = location[0]
        self.y = location[1]
        self.width = template.shape[0]
        #Note ROI will always show vertical bars as the template has been transposed for horizontal matches
        self.horizontal = horizontal
        #region of interest
        self.roi = image[self.y:self.y+self.width, self.x:self.x+self.width]
        #will be calculated by analyse
        self.tilt = None
        self.ccor = None
        self.spline = None
        self.locations = None
        #never just read this or you will miss tilt!
        self.spacing = None
        
        #will be set by fitting
        self.group = None
        self.element = None
        
        # If analysis fails this will say why
        self.exception = None
        
    def set_group_and_element(self,group,element):
        """Used to set the group and element number."""
        
        assert type(group) is int, 'Group must be an integer between 1 and 7'
        assert type(element) is int, 'Element must be an integer between 1 and 6'
        assert group>=1 and group<=7, 'Group must be an integer between 1 and 7'
        assert element>=1 and element<=6, 'Element must be an integer between 1 and 6'
        self.group=group
        self.element=element
    
    def get_separation_px(self):
        """This returns the separation in pixels from the centre
        of one bar to the centre of the next bar. Use this not
        the spacing parameter as this accounts for tilt.
        """
        if self.spacing is not None and self.tilt is not None:
            return self.spacing/np.cos(self.tilt)
        else:
            return None
        
    def get_lp_per_mm(self):
        """If group and element are set, this returns the
        number of line pairs per mm for this group"""
        if self.group is not None and self.element is not None:
            return 2**(self.group + (self.element-1)/6 )
        else:
            return None
    
    def get_normalised_ccor(self):
        """returns the normalised cross correlation of the average
        profile of the region of interest, with the provile of the
        central bar"""
        if self.ccor is not None:
            nccor = self.ccor-min(self.ccor)
            nccor /= max(nccor)
        else:
            nccor = None
        return nccor
        
    
    def overlap_x(self,match):
        """Return overlap in x (ignoring y) between this match and another match"""
        return self.overlap_1d(match,x=True)
    
    def overlap_y(self,match):
        """Return overlap in y (ignoring x) between this match and another match"""
        return self.overlap_1d(match,x=False)
    
    def overlap_1d(self,match,x=True):
        """Return overlap in x or y (ignoring the other direction) between this match and another match
        
        #if x1,x2 are the start of the the matches for this match and the input respectively
        #and w1,w2 are the widths of these regions in pixels
        Draw four positions, indicating the edges of the regions (i.e. x1, 
        x1+w1, x2, x2+w2).  The smallest distance between a starting edge (x1
        or x2) and a stopping edge (x+w) gives the overlap.  This will be
        one of the four values in the min().  The overlap can't be <0, so if
        the minimum is negative, return zero.
        """
        if x:
            x1 = self.x
            x2 = match.x
        else:
            x1 = self.y
            x2 = match.y
        
        return max(min(x1+ self.width - x2, x2 + match.width - x1, self.width, match.width), 0)
    
    def calculate_tilt(self,profile):
        """Calculate the tilt of the element given a line profile through the image for a target"""
        
        # profile_target is a 1px-high image with 3 bars in it
        profile_target = profile[np.newaxis, self.width//14:-self.width//14].astype(np.uint8)
        # We correlate the thin target with each row of the image, to
        # recover the tilt of the image.
        slant = cv2.matchTemplate(self.roi[self.width*3//14:-self.width*3//14, :],
                                  profile_target.astype(np.uint8), cv2.TM_CCOEFF_NORMED)
        ## threshold and centre of mass for each row
        slant -= slant.min(axis=1)[:,np.newaxis]
        slant /= slant.max(axis=1)[:,np.newaxis]
        slant -= 0.8
        slant[slant < 0] = 0
        #for each row multiply the pixel number by the intensity and sum.
        #divide through by the sum of the row do recover pixel number
        shifts = np.sum(slant * np.arange(slant.shape[1]), axis=1)/np.sum(slant, axis=1)
        # fit the peak in each row to find the angle
        gradient, offset = np.polyfit(np.arange(len(shifts)), shifts, 1)
        return np.arctan(gradient)
        
    
    def analyse(self,spline=False):
        """Calculate the precise period for this group of bars, from autocorrelation
        It also correlates each row of the image with the average row, which allows
        us to correct the values for tilt.
        """
        
        # Important note for this section:
        # The matched imaged size is approximatly 7 times the width of one bar.
        # This gives the 3 black lines, two white arease between, and a white area either side
        # As such an odd number over 14 represents the approximate centre of one of these regions
        # hence all the //14's in the code
        
        #Records whether spline fitting was used or simplistic peak finding
        self.spline_analysis = spline
        try:
            # average over the bars, ignoring the ends to create a line profile
            av_profile = np.mean(self.roi[self.width*3//14:-self.width*3//14, :], axis=0) 
            self.tilt = self.calculate_tilt(av_profile)

            # cut out the profile of the central bar and convole with the whole profile to get correlation
            centre = av_profile[self.width*5//14:self.width*9//14]
            self.ccor = np.convolve(av_profile, centre[::-1] - np.mean(centre), mode='valid')
            self.ccor_offset = (self.width-len(self.ccor)+1)//2

            #find peaks and troughs in the cross correlation
            peaks = np.logical_and(np.diff(self.ccor[:-1])>0,np.diff(self.ccor[1:])<0)
            troughs = np.logical_and(np.diff(self.ccor[:-1])<0,np.diff(self.ccor[1:])>0)
            #should be 3 peaks and 2 troughs
            assert np.sum(peaks)==3, 'wrong number of peaks'
            assert np.sum(troughs)==2, 'wrong number of troughs'

            #calculate rough locations to nearest pixel
            rough_locations = 1+np.arange(len(peaks))[np.logical_or(peaks,troughs)]
            #find location from the point and nearest neighbours
            self.locations = []
            for loc in rough_locations:
                section = np.abs(self.ccor[loc-1:loc+2])
                section -=np.min(section)
                subpx_loc = np.sum(np.arange(loc-1,loc+2)*section)/np.sum(section) + self.ccor_offset
                self.locations.append(subpx_loc)
            self.spacing = 2*np.mean(np.diff(self.locations))
        except Exception as ex:
            self.exception = ex
            print("\n\nOne match faild due to exception: {}\n\n".format(ex))


def template(n):
    """Generate a template image of three vertical bars, n x n pixels
    
    NB the bars occupy the central square of the template, with a margin
    equal to one bar all around.  There are 3 bars and 2 spaces between,
    so bars are m=n/7 wide.
    
    returns: n x n numpy array, uint8
    """
    n = int(n)
    template = np.ones((n, n), dtype=np.uint8)
    template *= 255
    
    for i in range(3):
        template[n//7:-n//7,  (1 + 2*i) * n//7:(2 + 2*i) * n//7] = 0
    return template

def find_elements(input_image,
                  template_fn=template, 
                  scale_increment=1.04,
                  n_scales=100,
                  return_all=False,verbose=False):
    """Use a multi-scale template match to find groups of 3 bars in the image.
    
    Inputs:
    
    image: a 2D uint8 numpy array in which to search
    template_fn: a function to generate a square uint8 array, which takes one
        argument n that specifies the side length of the square.
    scale_increment: the factor by which the template size is increased each
        iteration.  Should be a floating point number a little bigger than 1.0
    n_scales: the number of sizes searched.  The largest size is half the image
        size.
    
    Outputs:
    elements: a list match objects.
    all_matches (optional): same as elements, but all matches including overlapping and poor matches
    
    """
    all_matches = []
    elements = []
    
    for transposed in [False,True]:
        if transposed:
            image=input_image.T
        else:
            image=input_image
        
        #Look for matches with lots of templates of varying size
        matches = []
        start = np.log(image.shape[0]/2)/np.log(scale_increment) - n_scales
        if verbose:
            print("Searching for targets", end='')
        for nf in np.logspace(start, start + n_scales, base=scale_increment):
            if nf < 28:  #There's no point bothering with tiny boxes...
                continue
            templ = template(nf) #NB n is rounded down from nf
            res = cv2.matchTemplate(image,templ,cv2.TM_CCOEFF_NORMED)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            #create a USAFmatch objct for each match (horizontal=transposd)
            matches.append(USAFmatch(image,max_val,max_loc,templ,transposed))
            if verbose:
                print('.', end='')
        if verbose:
            print("done")

        # Take the matches at different scales and filter out the good ones
        scores = np.array([m.score for m in matches])
        threshold_score = (scores.max() + scores.min()) / 2
        filtered_matches = [m for m in matches if m.score > threshold_score]

        # Group overlapping matches together, and pick the best one
        unique_matches = []
        # loop over the filtered matches untill all are moved into unique_matches or deleted or overlapping
        while len(filtered_matches) > 0:
            #start a new group of overlapping regiouns
            current_group = []
            #pop out a match from filtered ready
            new_matches = [filtered_matches.pop()]
            #Loop looking for matches that overlap the current match until no new matches are found
            while len(new_matches) > 0: 
                current_group += new_matches
                new_matches = []
                for m1 in filtered_matches:
                    for m2 in current_group:
                        #calculate overlap
                        overlap = m1.overlap_x(m2)*m1.overlap_y(m2)
                        if overlap > 0.5 * min(m1.width, m2.width)**2:
                            new_matches.append(m1)
                            filtered_matches.remove(m1)
                            break
            # Now we should have current_group full of overlapping matches.  Pick
            # the best one.
            best_score_index = np.argmax([m.score for m in current_group])
            unique_matches.append(current_group[best_score_index])
    
        all_matches += matches
        elements += unique_matches
    
    if return_all:
        return elements, all_matches
    else:
        return elements

def assign_periods(elements, g=7, e=6,smallest=True):
    """Assign a group number and element number to all input elements.
    g and e should be set to the smallest element found in the image
    Default is group 7 element 6
    """

    if smallest:
        smallest_spacing = min([element.spacing for element in elements])
    else:
        largest_spacing = max([element.spacing for element in elements])
        
    xs = []
    for element in elements:
        # The sizes ought to be quantised in the sixth root of 2
        if smallest:
            #delta_el: number of elements larger than smallest
            delta_el = np.round(np.log2(element.spacing/smallest_spacing)*6)
        else:
            delta_el = np.round(np.log2(element.spacing/largest_spacing)*6)
        
        # thinking of the elements 6,5,4,3,2,1 as number 0,1,2,3,4,5
        # we add delta_el to get the new number
        num = 6-e+delta_el
        # Element number in USAF is then
        el = int(6 - num%6)
        # and group
        group = int(g - num//6)
        element.set_group_and_element(group,el)

def fit_elements(elements):
    '''Fits a straight line to the size of each elemment in pixels and in mm'''
    sep_in_px = []
    sep_in_mm = []
    for element in elements:
        sep_in_mm.append(1/element.get_lp_per_mm())
        # Using get separation ingludes tilt
        sep_in_px.append(element.get_separation_px())


    fit_coef,fit_cov = np.polyfit(sep_in_mm,sep_in_px,1,cov=True)
    fit_coef_rel_uncert = np.sqrt(np.diag(fit_cov))/fit_coef

    px_per_mm = fit_coef[0]
    um_per_px = 1000/px_per_mm
    um_per_px_uncert =um_per_px*fit_coef_rel_uncert[0]
    
    return {'fit_coef':fit_coef,
            'fit_cov':fit_cov,
            'um_per_px':um_per_px,
            'um_per_px_uncert':um_per_px_uncert
           }

def format_with_uncertainty(num,uc,dp=2):
    '''Print the number and associated uncertainty with nice rounding'''
    n = np.ceil(np.log10(uc))-dp
    uc = 10**n*np.round(uc/(10**n))
    num = 10**n*np.round(num/(10**n))
    if n<0:
        fmt = '%%.%df +/- %%.%df'%(-n,-n)
    else:
        fmt = '%.0f +/- %.0f'

    return fmt%(num,uc)

def plot_matches(parameters):
    """Plot the image, then highlight an numbers the elements."""
    image = parameters['image']
    f, ax = plt.subplots(1,1)
    if len(image.shape)==2:
        ax.imshow(image, cmap='gray')
    elif len(image.shape)==3 and image.shape[2]==3 or image.shape[2]==4:
        ax.imshow(image)
    else:
        raise ValueError("The image must be 2D or 3D")
        
    for element in parameters['matched_elements']:
        if element.horizontal:
            ax.add_patch(matplotlib.patches.Rectangle((element.y,element.x),
                                                      element.width, element.width, 
                                                      fc='none', ec='blue'))
            ax.text(element.y, element.x+.9*element.width,
                 "%d,%d"%(element.group,element.element), fontsize=8, color='blue')
        else:
            ax.add_patch(matplotlib.patches.Rectangle((element.x,element.y),
                                                      element.width, element.width, 
                                                      fc='none', ec='red'))
            ax.text(element.x, element.y+.9*element.width,
                 "%d,%d"%(element.group,element.element), fontsize=8, color='red')
    return f

def plot_regression(parameters):
    '''Plots the size in pixels and mm for each group and the straight line of best fit'''
    
    f, (ax,ax2) = plt.subplots(2,1)
    ax2.set_xlabel('Spacing [mm]')
    ax.set_ylabel('Spacing [px]')
    ax2.set_ylabel('Spacing residuals [px]')
    minx = 1e6
    maxx = -1e6
    for element in parameters['matched_elements']:
        if element.horizontal:
            fmt='bx'
        else:
            fmt='r+'
        in_mm = 1/element.get_lp_per_mm()
        plot_point = ax.plot(in_mm, element.get_separation_px(), fmt)
        if element.horizontal:
            horz_marker = plot_point
        else:
            vert_marker = plot_point
        ax2.plot(in_mm, element.get_separation_px()-(parameters['fit_coef'][1] + parameters['fit_coef'][0]*in_mm), fmt)
        minx = min(minx,in_mm)
        maxx = max(maxx,in_mm)
    xline = np.asarray([minx,maxx ])
    yline = parameters['fit_coef'][1] + parameters['fit_coef'][0]*xline
    fitline = ax.plot(xline,yline,'k-')
    ax.legend((horz_marker[0],vert_marker[0],fitline[0]),('Horiz. match','Vert. match','Fit'),loc='best',ncol=2)
    return f

def plot_element(element,ax,legend):
    '''Plots the cross-correlation of the central bar of an element with the element
    on top of the image of the element. Also highlights the edges of the bars.
    Exact placement of the bars is less acurate than the spacing'''
    if element.horizontal:
        ax.imshow(element.roi.T,cmap='gray')
    else:
        ax.imshow(element.roi,cmap='gray')

    #positions are calculated relative to
    width = element.width
    ccor_y = (.7-.4*element.get_normalised_ccor())*width
    ccor_x = element.ccor_offset+np.arange(len(ccor_y))
    #centre line
    ccor_y_centre = .5*width*np.ones(len(ccor_x))
    half_bar_w = element.get_separation_px()/4
    tilt_offset = np.sin(element.tilt)*2.5/7*width
    
    if element.horizontal:
        ax.plot(ccor_y,ccor_x,'g', label='cross-correlation')
        ax.plot(ccor_y_centre,ccor_x,'r-',label='cross-corr. max/min')
    else:
        ax.plot(ccor_x, ccor_y,'g-', label='cross-correlation')
        ax.plot(ccor_x, ccor_y_centre,'r-',label='cross-corr. max/min')
    ax.set_title('Gr. %d, El. %d'%(element.group,element.element))
    ax.axis('off')
    for n,location in enumerate(element.locations):
        locs = [location,location]
        
        if n%2:
            peak_line = [.5*width,.7*width]
        else:
            peak_line = [.3*width,.5*width]
        if element.horizontal:
            ax.plot(peak_line,locs,'r-')
        else:
            ax.plot(locs,peak_line,'r-')
        
        side = [1/7*width,6/7*width]
        side_loc = [location-half_bar_w-tilt_offset , location-half_bar_w+tilt_offset]
        if element.horizontal:
            ax.plot(side,side_loc,'b-')
        else:
            ax.plot(side_loc ,side,'b-')
        if n == 4:
            side_loc = [location+half_bar_w-tilt_offset , location+half_bar_w+tilt_offset]
            if element.horizontal:
                ax.plot(side,side_loc,'b-',label='edge')
            else:
                ax.plot(side_loc ,side,'b-',label='edge')
    if legend:
        ax.legend(loc='upper center', bbox_to_anchor=(0.05, -0.005), shadow=True, ncol=3)


def export_results(parameters,filename):
    """ Export the USAF analysis results to a PDF and yaml file
    
    Inputs:
      parameters is the parameter dictionary exported from 
        analyse_image or analyse_file
      filename is the output filename .pdf and .yaml will automatically
      be appended
    """
    
    elements = parameters['matched_elements']
    with PdfPages(filename+".pdf") as pdf:
        f,ax = plt.subplots(1,1)
        
        ax.text(0.1, 0.5,"USAF Resolution Target Analysis Output\n\n"
                "NOTE: Only trust these results if ALL elements\nare properly identified "+
                "on the following page.\nIf they aren't, adjust the smallest groups when\ncalling the analysis. "+
                "If numbers are matched as\ngroups you may need to blank them out in the\nimage.\n\n"+
                "Effective size of pixel = "+
                format_with_uncertainty(parameters['um_per_px'],parameters['um_per_px_uncert'] ,dp=1)+
                " um\nField of view = ("+
                format_with_uncertainty(parameters['field_of_view'][0],parameters['field_of_view_uncert'][0] ,dp=1)+
                ") x ("+
                format_with_uncertainty(parameters['field_of_view'][1],parameters['field_of_view_uncert'][1] ,dp=1)+
                ") um^2\n\nThese uncertainties are from the regression only and\nmay be an under estimation.\n\n",
            horizontalalignment='left',verticalalignment='center', transform=ax.transAxes)

        ax.axis('off')
        pdf.savefig(f)
        plt.close(f)
        f = plot_matches(parameters)
        pdf.savefig(f)
        plt.close(f)
        f = plot_regression(parameters)
        pdf.savefig(f)
        plt.close(f)
        n_el = len(elements)
        for n in range( int(np.ceil(n_el/6)) ):
            remaining = (n_el-n*6)
            f,ax = plt.subplots(2,3)
            for m in range( min(6,remaining) ):
                if m==4:
                    legend=True
                else:
                    legend=False
                plot_element(elements[n*6+m],ax[m//3,m%3],legend=legend)
            #Turn of axes for remaing plots
            while m < 6:
                ax[m//3,m%3].axis('off')
                m +=1
            
            pdf.savefig(f)
            plt.close(f)

    with open(filename+".yaml",'w') as file:
        file.write(yaml.dump(parameters))

def analyse_image(image,g=7,e=6,smallest=True,verbose=False):
    """Find USAF groups in the image and fit them to determine magnification.
    
    This is the top-level function that you should call to analyse an image.
    The image should be a 2D numpy array. You can use the function
    analyse_file if you want to input a filename.
    
    Optional inputs g and e set the group and element number of the
    smallest element found. Group 7 element 6 is assumed. Check the output
    and re-run if group/element is not correct
    
    Optional input smallest can be set to False (default=True). In this case
    g and e will refer to the largest not the smallest element
    
    Output is a parameter dictionary. You can use export results to
    save this dictionary and xport a human readable pdf."""
    
    if len(image.shape) == 3:
        gray_image = np.mean(image, axis=2).astype(np.uint8)
    else:
        gray_image = image.astype(np.uint8)
    
    #find elements (matches is every match to the template, elements
    #removes overlapping matches to the same region
    elements, matches = find_elements(gray_image, return_all=True,verbose=verbose)
    
    if verbose:
        print('Analysing %d elements'%len(elements))
    #Analyse the spacing of each matched element
    successfully_analysed = []
    for element in elements:
        element.analyse()
        if element.exception is None:
            successfully_analysed.append(element)
    elements = successfully_analysed
    #Assign group and element number to each element
    assign_periods(elements,g,e,smallest=smallest)
    
    #Fit the elements to calculate spacing and create output dictionary
    parameters = fit_elements(elements)
    parameters['image'] = gray_image
    parameters['matched_elements'] = elements
    parameters['all_matches'] = matches
    parameters['field_of_view'] = [ ind*parameters['um_per_px'] for ind in parameters['image'].T.shape] 
    parameters['field_of_view_uncert'] = [ ind*parameters['um_per_px_uncert'] for ind in parameters['image'].T.shape]
    
    return parameters
    
def analyse_file(filename,g=7,e=6,smallest=True,verbose=False):
    """Find USAF groups in the image and fit them to determine magnification.
    
    This is the top-level function that you should call to analyse an image file.
    If you want to analyse an numpy array tou can use the function analyse_image.
    
    Optional inputs g and e set the group and element number of the
    smallest element found. Group 7 element 6 is assumed. Check the output
    and re-run if group/element is not correct
    
    Optional input smallest can be set to False (default=True). In this case
    g and e will refer to the largest not the smallest element
    
    Output is a parameter dictionary. You can use export results to
    save this dictionary and xport a human readable pdf."""
    return analyse_image(imread(filename),g=g,e=e,smallest=smallest,verbose=verbose)


